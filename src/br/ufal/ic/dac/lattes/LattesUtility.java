package br.ufal.ic.dac.lattes;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;

public class LattesUtility {
	public static LattesUtility instance;

	private LattesUtility() {

	}

	public static LattesUtility getInstance() {
		if (instance == null) {
			instance = new LattesUtility();
		}
		return instance;
	}

	public void fillBlankIds(List<File> curriculums) {

		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder;

		try {

			dBuilder = dbFactory.newDocumentBuilder();
			for (File f : curriculums) {
				Document current = dBuilder.parse(f);

				String authorId = current.getDocumentElement().getAttribute("NUMERO-IDENTIFICADOR");

				if (authorId.equals("")) {
					authorId = f.getName().replace(".xml", "");
					current.getDocumentElement().setAttribute("NUMERO-IDENTIFICADOR", authorId);

					// writes xml
					TransformerFactory tFactory = TransformerFactory.newInstance();
					Transformer transformer = tFactory.newTransformer();

					DOMSource source = new DOMSource(current);
					StreamResult result = new StreamResult(new File(f.getAbsolutePath()));
					transformer.transform(source, result);
				}
			}

		} catch (Exception e) {

		}
	}

	public void renameFiles(List<File> curriculums) {

		try {

			for (File f : curriculums) {
				if (f.getName().contains("zip")) {

					File newFile = new File(f.getAbsolutePath().replace(".zip", ""));

					f.renameTo(newFile);
					System.out.println(newFile);
				}
			}
		} catch (Exception e) {

		}
	}

	public void processFiles() {

		File directory = new File("XML-Data/");
		directory.mkdirs();

		LattesUtility.getInstance().fillBlankIds(Arrays.asList(directory.listFiles()));
		// LattesUtility.getInstance().renameFiles(Arrays.asList(directory.listFiles()));

	}

}
