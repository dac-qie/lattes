package br.ufal.ic.dac.lattes;

public class LattesManager {

	public static void main(String[] args) {
		processXmlData();
		processJsonData();
		System.out.println("Finished!");
	}

	private static void processXmlData() {
		String[] input = { "artigos-aceitos", "artigos-publicados", "capitulos-de-livros-publicados",
				"livros-ou-capitulos", "livros-publicados-ou-organizados", "marca", "orientacoes-em-andamento",
				"outras-orientacoes-concluidas", "software", "formacao-academica" };

		for (String category : input)
			LattesXSL.getInstance().processXml(category);
	}

	private static void processJsonData() {
		String[] input = { "DADOS-GERAIS", "JORNAIS-OU-REVISTAS", "TRABALHOS-EM-EVENTOS" };

		for (String category : input)
			new LattesJson().processData(category);
	}

}